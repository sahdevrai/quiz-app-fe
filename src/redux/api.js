import axios from "axios";

const token=()=>{
return JSON.parse(localStorage.getItem('token')).jwt
}

export async function fetchQuizesApi(pg=1){
  return await axios.get(`http://localhost:3001/quiz/myQuizes/${pg}`,{
    headers:{
      jwt: JSON.parse(localStorage.getItem('token')).jwt
    }
})
}

export async function fetchUserApi(){
  return await axios.get("http://localhost:3001/user/verify",{
    headers:{
      jwt: JSON.parse(localStorage.getItem('token')).jwt
    }
})
}