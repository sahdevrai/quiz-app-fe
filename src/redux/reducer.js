const initialState = {
  user:[],
  userError:'',
  userLoading:false,
  quizes:[],
  quizesError:'',
  quizesLoading:false,
  totalQuizPages:0,
  totalQuizes:0
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
      
        case 'FETCH_USER_REQUEST':
          return {
            ...state,
            userLoading: true,
          }
        case 'FETCH_USER_SUCCESS':
          return {
            ...state,
            userLoading: false,
            user: action.payload,
            userError: ''
          }
         
        case 'FETCH_USER_FAILURE':
          return {
            userLoading: false,
            userError: action.payload,
            user:[]
          }
          case 'FETCH_MY_QUIZES_REQUEST':
            return {
              ...state,
              quizesLoading: true,
            }
          case 'FETCH_MY_QUIZES_SUCCESS':
            return {
              ...state,
              quizesLoading: false,
              quizes: action.payload[0],
              totalQuizPages:action.payload[1],
              totalQuizes:action.payload[2],
              quizesError: ''
            }
           
          case 'FETCH_MY_QUIZES_FAILURE':
            return {
              quizesLoading: false,
              quizesError: action.payload,
              quizes:[]
            }

      default: return state
    }
}

export default reducer
