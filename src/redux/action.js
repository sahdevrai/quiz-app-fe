import axios from 'axios'
import { fetchQuizesApi, fetchUserApi } from './api'



export const fetchMyQuizes =(pg=1) => {
    return (dispatch) => {
      if(JSON.parse(localStorage.getItem('token'))!==null){
      dispatch(fetchMyQuizesRequest())

      async function fun(){
        try{
          let response=await fetchQuizesApi(pg)
          const events = response.data
          dispatch(fetchMyQuizesSuccess(events))  
        }
        catch(error){
          dispatch(fetchMyQuizesFailure(error.message))
        }
      }
      fun()  
      }
    }
  }

  
  

  export const fetchUser =() => {
    return (dispatch) => {
      if(JSON.parse(localStorage.getItem('token'))!==null){
      dispatch(fetchUserRequest())
      async function fun(){
        try{
        let events=await fetchUserApi()
        dispatch(fetchUserSuccess(events.data.result))
      }
      catch(error){
        dispatch(fetchUserFailure(error.message))
      }
    }
    fun()    
  }
    }
  }

  
  
  


export const fetchUserRequest = () => {
    return {
        type: 'FETCH_USER_REQUEST'
    }
}

export const fetchMyQuizesRequest = () => {
  return {
      type: 'FETCH_MY_QUIZES_REQUEST'
  }
}

export const fetchUserSuccess = events => {
    return {
      type: 'FETCH_USER_SUCCESS',
      payload: events
    }
}

export const fetchMyQuizesSuccess = events => {
  return {
    type: 'FETCH_MY_QUIZES_SUCCESS',
    payload: events
  }
}

export const fetchUserFailure = error => {
    return {
      type: 'FETCH_USER_FAILURE',
      payload: error
    }
}

export const fetchMyQuizesFailure = error => {
  return {
    type: 'FETCH_MY_QUIZES_FAILURE',
    payload: error
  }
}
