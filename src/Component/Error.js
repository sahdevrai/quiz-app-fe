import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

function Error() {
  const navigate=useNavigate()
  useEffect(() => {
    navigate('/auth')
     // eslint-disable-next-line
  }, [])
  
  return (
    <div></div>
  )
}

export default Error