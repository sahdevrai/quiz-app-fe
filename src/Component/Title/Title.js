import axios from 'axios'
import React, { useState } from 'react'
import { Modal, Button, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

function Title() {
  const navigate=useNavigate()
  const [isShow, invokeModal] = useState(false)
  const [title, setTitle] = useState('')

  const initModal = () => {
    return invokeModal(!isShow)
  }

  const createQuiz=()=>{

    setTitle(title.trim())

    if(title.length<5)
    {
      toast.error('Length cannot be smaller than 5')
    }

    axios.post("http://localhost:3001/quiz/create",{title:title},{
                headers:{
                  jwt: JSON.parse(localStorage.getItem('token')).jwt
                }
            })
    .then((data)=>{
      //console.log(data.data)
      toast.success("Quiz initialised seccessfully")
      initModal()
      navigate(`/create/${data.data}`)
    })
    .catch((err)=>{
      //console.log(err)
      toast.error(err.message)
    })
  }
 

  return (
    <>
      <Button variant="primary" onClick={()=>initModal()}>
        Create A Quiz
      </Button>
      <Modal show={isShow}>
        <Modal.Header closeButton onClick={()=>initModal()}>
          <Modal.Title>Add a title</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Title of Quiz</Form.Label>
              <Form.Control
                type="text"
                placeholder='title'
                autoFocus
                value={title}
                onChange={(e)=>setTitle(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={()=>createQuiz()}>
            Save
          </Button>
          <Button variant="danger" onClick={()=>initModal()}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      <ToastContainer/>
    </>
  )
}
export default Title