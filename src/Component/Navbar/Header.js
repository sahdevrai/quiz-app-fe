import React from 'react'
import { connect } from 'react-redux'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { useNavigate } from 'react-router-dom';

export const Header = (props) => {
  const navigate=useNavigate();

  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand onClick={()=>navigate('/')}>Quizzy</Navbar.Brand>
        {['start','finish','quiz'].indexOf(window.location.pathname.split('/')[1])==-1 && <><Navbar.Toggle aria-controls="navbarScroll" />
       <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '120px' }}
            navbarScroll
          >
            <Nav.Link onClick={()=>navigate('/')}>Home</Nav.Link>
            <Nav.Link onClick={()=>navigate('/myQuizes')}>My Quizes</Nav.Link>
            <Nav.Link 
            onClick={()=>{localStorage.removeItem('token');navigate('/auth')}}>Sign Out</Nav.Link>
          </Nav>
        </Navbar.Collapse></>}
      </Container>
    </Navbar>
  )
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(Header)