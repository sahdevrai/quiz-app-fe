import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Header from '../Navbar/Header'

const Finish = () => {
  const [correct, setCorrect] = useState(0)
  const [total, setTotal] = useState(0)

  const navigate=useNavigate()
  useEffect(() => {
    if(localStorage.getItem("correctAnswer")===null || localStorage.getItem("answerDetails")===null)
    {
      navigate('/')
    }

    let filledAnswer=JSON.parse(localStorage.getItem("answerDetails")),correctAnswer=JSON.parse(localStorage.getItem("correctAnswer"));

    //console.log(filledAnswer)
    //console.log(correctAnswer)

    let cnt=0
    Object.keys(filledAnswer).forEach(value=>{
      if(filledAnswer[value].join()===correctAnswer[value].join())
      {
        //console.log(11212312)
        cnt+=1;
      }
    })
    setCorrect(cnt)
    setTotal(Object.keys(correctAnswer).length)

  }, [])
  
  return (
    <div>
      <Header/>
      <h1 style={{marginTop:"100px"}}>You have successfully submitted the quiz!</h1>
      <h4>You have marked {correct} out of {total} correctly</h4>

      <div 
      className='btn btn-outline-primary'
      onClick={()=>navigate('/auth')}>Go to Login page</div>
    </div>
  )
}

export default Finish