import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import Header from '../Navbar/Header'

const Start = () => {
  const navigate=useNavigate()
  useEffect(() => {
    localStorage.removeItem('answerDetails')
    localStorage.removeItem('correctAnswer')
    localStorage.removeItem('question')
    localStorage.removeItem('totalQuestions')

    
  }, [])
  
  return (
    <div>
      <Header/>
      <h2 style={{"marginTop":'100px'}}>
        Are you ready to start the quiz
      </h2>
      <div 
      onClick={()=>{navigate(`/quiz/${window.location.pathname.split('/')[2]}`)}}
      className='btn btn-outline-primary'
      style={{"width":'200px',"height":'50px',"padding":'12px'}}>
        Go to Quiz
      </div>
    </div>
  )
}

export default Start