import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { fetchMyQuizes, fetchUser } from '../../redux/action'
import Header from '../Navbar/Header'
import {CircularProgress} from '@mui/material'
import Error from '../Error'
import { Card } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { Modal, Button, Form } from 'react-bootstrap'
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


export const MyQuizes = ({state,fetchUser,fetchMyQuizes}) => {

  const navigate=useNavigate()

  useEffect(() => {
    fetchMyQuizes()
    fetchUser()
     // eslint-disable-next-line
  }, [])

  const [isShow, invokeModal] = useState(false)
  const [deleteQuizId, setDeleteQuizId] = useState(-1)

  const initModal = () => {
    return invokeModal(!isShow)
  }

  //console.log(state)

  const deleteQuiz=(id)=>{
    axios.delete(`http://localhost:3001/quiz/${id}/delete`,{
                headers:{
                  jwt: JSON.parse(localStorage.getItem('token')).jwt
                }
            })
        .then((data)=>{
            //console.log(data)
            invokeModal(!isShow)
            toast.success('Deleted Successfully')
            fetchMyQuizes()
        })
        .catch(error => {          
            //console.log(error)
        })
  }
  
  return (state.userLoading)?(<CircularProgress/>):(state.userError)?(<Error/>):(
    <div>
      <Header/>

      {
        state.totalQuizes===0 && <h1>No Quiz Added</h1>
      }
     
<div style={{"margin":'auto',"marginTop":'100px',"minWidth":'400px',"width":'50%'}}>
    {
    state.quizes.map((value,key)=>(
      <Card 
      key={key}
      style={{"marginTop":'40px'}}>
      <Card.Body 
      className='d-flex flex-row mb-3'>
      <Card.Title>{value.title}</Card.Title>
        </Card.Body>
        <div style={{"marginTop":'-30px'}}>
        <Card.Body className='d-flex flex-row mb-3'>
        <Card.Link 
        onClick={()=>navigate(`/start/${value.link}`)}style={{"cursor":'pointer'}}>Go to Quiz</Card.Link>
        <Card.Link 
        onClick={()=>navigator.clipboard.writeText(`${window.location.origin}/start/${value.link}`)}
        style={{"cursor":'pointer'}}>Copy Link</Card.Link>
        <Card.Link 
        onClick={()=>{setDeleteQuizId(value.id); initModal()}}
        style={{"cursor":'pointer'}}>Delete quiz</Card.Link>
        </Card.Body>
      <Modal show={isShow}>
        <Modal.Header closeButton onClick={()=>initModal()}>
          <Modal.Title>Delete Quiz</Modal.Title>
        </Modal.Header>
        <Modal.Body>
       Do you want to delete the quiz?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={()=>deleteQuiz(deleteQuizId)}>
            Delete
          </Button>
          <Button variant="danger" onClick={()=>initModal()}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
        </div>
        <ToastContainer/>
    </Card>
    ))
    }
  </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return{
    state:state
  }
}

const mapDispatchToProps = (dispatch)=>{
  return{
    fetchUser: () => dispatch(fetchUser()),
    fetchMyQuizes: (pg) => dispatch(fetchMyQuizes(pg)),
    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyQuizes)