import { Pagination, Stack } from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Card, ListGroup } from 'react-bootstrap'
import { connect } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';import Header from '../Navbar/Header'

export const TakeQuiz = (props) => {
  const navigate=useNavigate()
  const [questionDetails, setQuestionDetails] = useState([])
  const [totalQuestions, setTotalQuestions] = useState(0)
  const [title, setTitle] = useState('')
  let [page, setPage] = useState(1)

  const [answer, setAnswer] = useState([])

  useEffect(() => {
    let questionsNumber;
    if(localStorage.getItem('question')===null)
    {
      localStorage.setItem('question',1)
      questionsNumber=1;
      setPage(1)
    }
    else{
      questionsNumber=localStorage.getItem('question')
      setPage(Number(questionsNumber))
    }
    axios.get(`http://localhost:3001/quiz/takeQuiz/${window.location.pathname.split('/')[2]}/${questionsNumber}`)
        .then(response => {
          const events = response.data
  
          // console.log(events)
          setQuestionDetails(events.questionDetails[0])
          setTotalQuestions(events.totalQuestions)
          localStorage.setItem("totalQuestions",events.totalQuestions)
          // localStorage.setItem("correct",0)

          setTitle(events.title)

    
    if(localStorage.getItem("answerDetails")!==null && JSON.parse(localStorage.getItem("answerDetails"))[questionsNumber]!==null && JSON.parse(localStorage.getItem("answerDetails"))[questionsNumber]!==undefined)
    {
      console.log(questionsNumber)
      setAnswer([...JSON.parse(localStorage.getItem("answerDetails"))[questionsNumber]])
    }

    if(localStorage.getItem("correctAnswer")!==null)
    {
      // console.log(questionDetails)
      localStorage.setItem("correctAnswer",JSON.stringify({...JSON.parse(localStorage.getItem("correctAnswer")),[questionsNumber]:[...events.questionDetails[0].answer].sort()}))
    }
    else{
      localStorage.setItem("correctAnswer",JSON.stringify({[questionsNumber]:[...events.questionDetails[0].answer].sort()}))
    }
        })
        .catch(error => {
          setTimeout(() => {
            toast.error("This quiz doesn't exist")
          }, 3000);
          navigate(`/start/${window.location.pathname.split('/')[2]}`)
        })
 
  }, [])
  const handleChange = (e, p) => {
    localStorage.setItem('question',p)
    setPage(p);
    setAnswer([])
    axios.get(`http://localhost:3001/quiz/takeQuiz/${window.location.pathname.split('/')[2]}/${p}`)
        .then(response => {
          const events = response.data
  
          // console.log(events)
          setQuestionDetails(events.questionDetails[0])
          setTotalQuestions(events.totalQuestions)
          setTitle(events.title)

          if(JSON.parse(localStorage.getItem("answerDetails"))[p]!==null && JSON.parse(localStorage.getItem("answerDetails"))[p]!==undefined)
          {
            // console.log(JSON.parse(localStorage.getItem("answerDetails"))[p])
            setAnswer([...JSON.parse(localStorage.getItem("answerDetails"))[p]])
          }

          localStorage.setItem("correctAnswer",JSON.stringify({...JSON.parse(localStorage.getItem("correctAnswer")),[p]:[...events.questionDetails[0].answer].sort()}))
        })
        .catch(error => {

          navigate('/start')
          
        })
  };
  // console.log([questionDetails,totalQuestions,title])
  const handleAnswer=(key)=>{
    let tmp;
    if(questionDetails.typeOfQuestion==="multiple"){
      if(answer.indexOf(key)===-1)
      {
        tmp=[...answer,key]
        setAnswer([...answer,key])
      }
      else{
        tmp=answer.filter(item => item !== key)
        setAnswer(answer.filter(item => item !== key))
      }
    }
    else
    {
      tmp=[key]
      setAnswer([key])
    }
    if(localStorage.getItem("answerDetails")!==null)
    {
      localStorage.setItem("answerDetails",JSON.stringify({...JSON.parse(localStorage.getItem("answerDetails")),[page]:[...tmp].sort()}))

    }
    else{
      localStorage.setItem("answerDetails",JSON.stringify({[page]:[...tmp].sort()}))
    }
    
  }
  // console.log(answer)
  return (Object.keys(questionDetails).length>0)?(
    <div>
      <Header/>
      <h1>{title}</h1>
      <div style={{width:'50%',margin:'auto',marginTop:'100px'}}>
      <Card>
      <Card.Body className="d-flex flex-row">{"Q. "+questionDetails.question}</Card.Body>
    </Card>
    {/* {console.log(answer)} */}
    <Card style={{marginTop:'30px' }}>
      <ListGroup variant="flush">
        {
          Object.keys(questionDetails.options).map((value,key)=>(
          <ListGroup.Item 
          key={key}
          className="d-flex flex-row"
          style={{backgroundColor:answer.indexOf(key)!==-1?"lightgreen":''}}
          onClick={()=>handleAnswer(key)}>
            {(key+1+". ")+questionDetails.options[value]}
          </ListGroup.Item>
          ))
        }
      </ListGroup>
    </Card>
    </div>
    <Stack
    alignItems={'center'}
    sx={{m:2}}>
    <Pagination
        count={totalQuestions}
        size="large"
        page={page}
        variant="outlined"
        shape="rounded"
        onChange={handleChange}
      />
    </Stack>
    <div 
    className='btn btn-outline-success'
    onClick={()=>navigate('/finish')}>Submit Quiz</div>
    <ToastContainer/>
    </div>
  ):''
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(TakeQuiz)