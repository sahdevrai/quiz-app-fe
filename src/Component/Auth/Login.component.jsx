import React, { useEffect, useState } from 'react'
import './Auth.css'
import Stack from '@mui/material/Stack'
import TextField from '@mui/material/TextField';
import axios from 'axios'
import Joi from 'joi';
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchUser } from '../../redux/action';



const Login=({fetchUser})=>{
  const navigate=useNavigate();


  useEffect(() => {
    if(localStorage.getItem('token')!==null && localStorage.getItem('token')!==undefined &&
        localStorage.getItem('token')!=='' && JSON.parse(localStorage.getItem('token')).expiry>new Date().getTime()){


    fetchUser();
    navigate('/')
  }
    // eslint-disable-next-line
  }, [])
  
  const [loginForm, setLoginForm] = useState({
    email:'',
    password:''
  })
  const schema = Joi.object({
    email: Joi.string().email({ tlds: { allow: false } }).max(30).required(),
    password: Joi.string().min(8).max(30).required()
  });


  const handleSubmit=()=>{
    setLoginForm({
      ...loginForm,
      email:loginForm.email.trim().toLowerCase()
    })
        const result = schema.validate(loginForm);
    const { error } = result;

    if (!error) {
        axios.post("http://localhost:3001/user/login", {...loginForm}).then((data)=>{
 
             localStorage.setItem('token',JSON.stringify({jwt:data.data.jwt,expiry:new Date().getTime()+5*3600000,role:data.data.role}))
             toast.success('Logged in successfully')

             setTimeout(() => {
             navigate('/')
             }, 3000);
            
         }).catch((err)=>{
             toast.error(err.response.data.message)
         }) 
 
     } else {
    toast.error(result.error.message)
     }
}

  return(
    <div>
      <br />
      <h1 
      style={{'fontFamily':'monospace'}}>
        Welcome Back:)
      </h1>
      <TextField 
      id="standard-basic1" 
      label="Email" 
      type={'email'}
      variant="standard" 
      sx={{mt:2,width: 300}} 
      value={loginForm.email}
      onChange={(e)=>setLoginForm({...loginForm,email:e.target.value})}/>
    
      <TextField 
      id="standard-basic2" 
      label="Password" 
      variant="standard" 
      type={'password'} 
      sx={{mt:2,mb:2,width: 300}} 
      value={loginForm.password} 
      onChange={(e)=>setLoginForm({...loginForm,password:e.target.value})}/>
      <br />
    
      <Stack 
    direction={'row'}
    alignItems={'center'}
    justifyContent={'center'}
    spacing={2}>
      <div 
      className='btn btn-outline-primary'
      onClick={()=>handleSubmit()}>LogIn</div>

    </Stack>
      <ToastContainer/>
    </div>
  )
}



const mapStateToProps = (state) => {
  return {
      state: state
    }
}

const mapDispatchToProps = dispatch => {
  return{
      fetchUser: () => dispatch(fetchUser()),
    }
}


  
export default connect(mapStateToProps,mapDispatchToProps)(Login)