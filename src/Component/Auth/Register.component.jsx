import React, { useState } from 'react'
import './Auth.css'
import Stack from '@mui/material/Stack'
import TextField from '@mui/material/TextField';
import axios from 'axios'
import Joi from 'joi';
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


const Register=({formStatus})=>{
  const [registerForm, setRegisterForm] = useState({
    name:'',
    email:'',
    password:''
})

const schema = Joi.object({
  email: Joi.string().email({ tlds: { allow: false } }).max(30).required(),
  password: Joi.string().min(8).max(30).required(),
  name:Joi.string().min(3).max(20).required(),
});

const handleSubmit=()=>{
  setRegisterForm({
    ...registerForm,
    name:registerForm.name.trim().toLowerCase(),
    email:registerForm.email.trim()
  })
  
  const result = schema.validate(registerForm);
  //console.log(result); 
  const { error } = result;

  if (!error) {
      axios.post("http://localhost:3001/user/register", {...registerForm}).then((data)=>{

         
           toast.success('Signed Up successfully, Now Login')

           setTimeout(() => {
          formStatus('login')

           }, 3000);
          
       }).catch((err)=>{
           toast.error(err.response.data.message)
       }) 

   } else {
  toast.error(result.error.message)
   }
}


  return(
    <div>
      <br />
       <h1 
       style={{'fontFamily':'monospace'}}>
        Register
       </h1>
    <TextField 
    id="standard-basic1" 
    label="Name" 
    variant="standard" 
    sx={{mt:2,width: 300}} 
    value={registerForm.name} 
    onChange={(e)=>setRegisterForm({...registerForm,name:e.target.value})}/>

    <TextField 
    id="standard-basic2" 
    label="Email" 
    variant="standard" 
    type={"email"} 
    sx={{mt:2,width: 300}} 
    value={registerForm.email} 
    onChange={(e)=>setRegisterForm({...registerForm,email:e.target.value})}/>

    <TextField 
    id="standard-basic3" 
    label="Password" 
    variant="standard" 
    type={'password'} 
    sx={{mt:2,mb:2,width: 300}} 
    value={registerForm.password} 
    onChange={(e)=>setRegisterForm({...registerForm,password:e.target.value})}/>
    <br />


    
    <Stack 
    direction={'row'}
    alignItems={'center'}
    justifyContent={'center'}
    spacing={2}>
      <div 
      className='btn btn-outline-success'
      onClick={()=>handleSubmit()}>Sign Up</div>

    </Stack>
   
    <ToastContainer/>
    </div>
  )
}

export default Register