import { CircularProgress } from '@mui/material'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { fetchMyQuizes, fetchUser } from '../../redux/action'
import Error from '../Error'
import Header from '../Navbar/Header'
import Title from '../Title/Title'

export const MainPage = ({state,fetchUser,fetchMyQuizes}) => {
  const navigate=useNavigate()
  useEffect(() => {
    fetchMyQuizes()
    fetchUser()
     // eslint-disable-next-line
  }, [])
  return (state.userLoading)?(<CircularProgress/>):(state.userError)?(<Error/>):(
    <div>
      <Header/>
      <div style={{'marginTop':'10%'}}>
        <h1>
          Make quiz in the easiest Way
        </h1>
        <div>
          <div>
          <Title/>
          </div>
          <h1>
          <button 
          className='btn btn-outline-success'
          onClick={()=>navigate('/myQuizes')}>See previous quizes</button>
          </h1>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return{
    state:state
  }
}

const mapDispatchToProps = (dispatch)=>{
  return{
    fetchUser: () => dispatch(fetchUser()),
    fetchMyQuizes: (pg) => dispatch(fetchMyQuizes(pg)),
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(MainPage)