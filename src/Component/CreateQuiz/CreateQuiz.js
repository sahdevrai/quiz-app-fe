import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import Header from '../Navbar/Header'
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { Button, Form, InputGroup } from 'react-bootstrap'
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import './CreateQuiz.css'

export const CreateQuiz = (props) => {
  const navigate=useNavigate()
  const [numberOfOptions, setNumberOfOptions] = useState(1)
  const [typeOfQuestion, setTypeOfQuestion] = useState('single')
  const [answer, setAnswer] = useState([])
  const [question, setQuestion] = useState("")
  const [options, setOptions] = useState({
    0:'',
    1:'',
    2:'',
    3:'',
    4:''
  })
  const [questionNo, setQuestionNo] = useState(0)


  useEffect(() => {
    axios.get(`http://localhost:3001/quiz/${window.location.pathname.split('/')[2]}/isAvailable`,{
                headers:{
                  jwt: JSON.parse(localStorage.getItem('token')).jwt
                }
            })
        .then(response => {
          const events = response.data
          
          setQuestionNo(events)
        })
        .catch(err=>{
          console.log(err)
          navigate('/')
          setTimeout(() => {
            toast.error(err.response.data.message)
          }, 1000);
        })
  }, [])
  console.log(answer)
  const mapanswer=(key)=>{
    if(typeOfQuestion==="multiple"){
      if(answer.indexOf(key)===-1)
      {
        setAnswer([...answer,key])
      }
      else{
        setAnswer(answer.filter(item => item !== key))
      }
    }
    else
    {
      setAnswer([key])
    }
  }

  const submit=()=>{
    if(questionNo==11)
    {
      toast.error("Maximum question limit reached")
      return 
    }
    setQuestion(question.trim());

    let op={}
    const tempOptions=Object.keys(options).filter(value=>{
      if(options[value].trim()==='' && answer.indexOf(Number(value))!==-1)
      {
        console.log(23)

        toast.error('Answer cannot be an empty option')
        return
      }
      if(options[value].trim()!=='')
      op={...op,[value]:options[value].trim()}
      return options[value].trim()!==''
    })
    if((typeOfQuestion==='single' && answer.length>1)||(answer.length>tempOptions.length))
    {
      toast.error('Select valid number of options')
      return 
    }

// console.log(tempOptions)
    if(tempOptions.length<=1 || answer.length===0)
    {
      toast.error('Fill all the compulsory fields')
      return
    }

    if(question.length<15)
    {
      toast.error('Question length cannot be less than 15')
      return
    }

    console.log([question,op,...answer,typeOfQuestion])
    axios.put(`http://localhost:3001/quiz/${window.location.pathname.split('/')[2]}/updateQuiz`,{question:question,options:op,answer:answer,typeOfQuestion:typeOfQuestion},{
                headers:{
                  jwt: JSON.parse(localStorage.getItem('token')).jwt
                }
            })
        .then(response => {
          const events = response.data
          
          console.log(events)
          setQuestionNo(questionNo+1)
          toast.success("Question added successfully")
          setNumberOfOptions(1)
          setTypeOfQuestion('single')
          setAnswer([])
          setQuestion("")
          setOptions({
            0:'',
            1:'',
            2:'',
            3:'',
            4:''
          })
        })
        .catch(err=>{
          // console.log(err)
          // navigate('/')
          setTimeout(() => {
            toast.error(err.response.data.message)
          }, 1000);
        })
  }

  const submitQuiz=()=>{
    axios.get(`http://localhost:3001/quiz/${window.location.pathname.split('/')[2]}/published`,{
                headers:{
                  jwt: JSON.parse(localStorage.getItem('token')).jwt
                }
            })
        .then(response => {
          const events = response.data
          
          console.log(events)

          setTimeout(() => {
            toast.success("Quiz Published")
          }, 1000);
          navigate('/')
          
        })
        .catch(err=>{
          // console.log(err)
          // navigate('/')
          setTimeout(() => {
            toast.error(err.response.data.message)
          }, 1000);
        })
  }
  return (
    <div>
      <Header/>

      <div 
      style={{"width":'50%',minWidth:'300px','margin':'auto',"marginTop":"100px",}}>
        <div>
        <Form onSubmit={(e)=>{e.preventDefault();submit();}}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Question {questionNo}</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter question" 
        onChange={(e)=>setQuestion(e.target.value)}
        value={question}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Options</Form.Label>
        {Array.from(Array(numberOfOptions).keys()).map((element,key)=>(
          <InputGroup className="mb-3" key={key}>
        <InputGroup.Text 
        id="basic-addon1" 
        onClick={()=>{mapanswer(key)}}
        style={ answer.indexOf(key)!==-1? { backgroundColor:'lightGreen',cursor:'pointer'} : {cursor:'pointer'}}>{key+1}</InputGroup.Text>
        <Form.Control 
        type="text"
        placeholder={"Option"+(key+1)} 
        onChange={(e)=>setOptions({...options,[key]:e.target.value})}
        value={options[key]}/>
        {console.log(options)}
      </InputGroup>
        ))
        }
      </Form.Group>{console.log(typeOfQuestion)}
      <Form.Select aria-label="Default select example" onChange={(e)=>{setTypeOfQuestion(e.target.value);setAnswer([])}}
      value={typeOfQuestion}>
      <option value="single">Single Choice answer</option>
      <option value="multiple">Multiple Choice answer</option>
    </Form.Select>
    <br />
    <div>
     <Button 
      variant="outline-primary" 
      onClick={numberOfOptions>=2?()=>{setOptions({...options,[numberOfOptions-1]:''});setNumberOfOptions(numberOfOptions-1);}:()=>{return}}
      style={{cursor:numberOfOptions<2?"not-allowed":''}}>
      <RemoveIcon/> 
      </Button>
      <Button 
      variant="outline-primary" 
      type="submit"
      style={{'margin':'0 30px 0'}}>
        Add Question
      </Button>
      <Button variant="outline-primary" 
      onClick={numberOfOptions<=4?()=>{setNumberOfOptions(numberOfOptions+1)}:()=>{return}}
      style={{cursor:numberOfOptions>4?"not-allowed":''}}>
      <AddIcon/> 
      </Button>
      </div>
    </Form>
    <br />
    <Button 
      variant="outline-primary" 
      onClick={()=>{submitQuiz()}}>
        Submit quiz
      </Button>
        </div>
      </div>
      <ToastContainer/>
    </div>
  )
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(CreateQuiz)