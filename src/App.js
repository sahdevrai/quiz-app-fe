import { Route, Routes } from 'react-router-dom';
import './App.css';
import AuthComponent from './Component/Auth/Auth.component';
import CreateQuiz from './Component/CreateQuiz/CreateQuiz';
import MainPage from './Component/MainPage/MainPage';
import Finish from './Component/Quizes/Finish';
import MyQuizes from './Component/Quizes/MyQuizes';
import Start from './Component/Quizes/Start';
import TakeQuiz from './Component/Quizes/TakeQuiz';

function App() {
  return (
    <div className="App">
      <Routes>
      <Route path="/auth" index element={<AuthComponent/>}/>
      <Route path="/" element={<MainPage/>}/>
      <Route path="/myQuizes" element={<MyQuizes/>}/>
      <Route path="/create/:no" element={<CreateQuiz/>}/>
      <Route path="/quiz/:no" element={<TakeQuiz/>}/>
      <Route path="/finish" element={<Finish/>}/>
      <Route path="/start/:id" element={<Start/>}/>
      </Routes>
    </div>
  );
}

export default App;
